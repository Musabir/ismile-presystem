package com.example.musabirmusabayli.ismile_presystem.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.activity.ChatActivity;
import com.example.musabirmusabayli.ismile_presystem.model.ChatModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatListAdapter extends BaseAdapter {
    Context context;
    ArrayList<ChatModel> chatModels;
    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    public ChatListAdapter(Context context, ArrayList<ChatModel> chatModels) {
        this.chatModels = chatModels;
        this.context = context;

    }

    @Override
    public int getCount() {
        return chatModels.size();
    }

    @Override
    public ChatModel getItem(int i) {
        return chatModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final LinearLayout contactsView;
        if (view == null) {
            contactsView = new LinearLayout(context);
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater) context.getSystemService(inflater);
            vi.inflate(R.layout.adapter_chat, contactsView, true);
        } else {
            contactsView = (LinearLayout) view;

        }
        final ChatModel chatModel = getItem(i);

        TextView profileName = contactsView.findViewById(R.id.profile_name);
        TextView lastMessage = contactsView.findViewById(R.id.last_msg);
        TextView unseenMessageCount = contactsView.findViewById(R.id.number_of_unread_message_count);
        TextView date = contactsView.findViewById(R.id.date);
        ImageView tick = contactsView.findViewById(R.id.tick);
        CircleImageView profileImage = contactsView.findViewById(R.id.profile_image);
        RelativeLayout unseenCountLyt = contactsView.findViewById(R.id.number_of_unread_message_lyt);
        View line = contactsView.findViewById(R.id.view);
        if (i > 0) {
            loadImage(chatModel.getProfileImage(), profileImage);
            line.setVisibility(View.VISIBLE);
            profileImage.setVisibility(View.VISIBLE);

        } else {
            contactsView.setEnabled(false);
            contactsView.setOnClickListener(null);
            line.setVisibility(View.INVISIBLE);
            profileImage.setVisibility(View.INVISIBLE);
        }

        profileName.setText(chatModel.getProfileName());
        lastMessage.setText(chatModel.getMessage());
        date.setText(chatModel.getDate());
        if (chatModel.isiSend() == true) {
            tick.setVisibility(View.VISIBLE);
            unseenCountLyt.setVisibility(View.GONE);
            if (chatModel.getStatus() == 0)
                tick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_single_tick));
            else if (chatModel.getStatus() == 1)
                tick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_double_tick_unseen));
            else if (chatModel.getStatus() == 2)
                tick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_double_tick_seen));
        } else {
            tick.setVisibility(View.GONE);
            if (chatModel.getNumberOfUnseenMessage() == 0)
                unseenMessageCount.setVisibility(View.GONE);
            else unseenMessageCount.setVisibility(View.VISIBLE);
            unseenMessageCount.setText(chatModel.getNumberOfUnseenMessage() + "");
            if (i > 0)
                unseenCountLyt.setVisibility(View.VISIBLE);
            else
                unseenCountLyt.setVisibility(View.GONE);


        }
        contactsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("lastseen","Online");
                intent.putExtra("name",chatModel.getProfileName());
                intent.putExtra("avatar",chatModel.getProfileImage());
                intent.putExtra("msisdn",chatModel.getMsisdn());
                intent.putExtra("isGroup",chatModel.isGroup());
                if(chatModel.isGroup()) {

                }

                context.startActivity(intent);
            }
        });
        return contactsView;
    }

    private void loadImage(String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(100, 100) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        imageView.setImageDrawable(circularBitmapDrawable);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {

                    }
                });

    }
}
