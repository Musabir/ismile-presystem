package com.example.musabirmusabayli.ismile_presystem.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.example.musabirmusabayli.ismile_presystem.model.UserModel;
import com.google.gson.Gson;

public class SharedPreferenceHandler {
    Context context;
    private static final String TAG = "SharedPreferenceHandler";
    public SharedPreferenceHandler(Context context) {
        this.context = context;

    }

    public void saveUserModel(UserModel userModel) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        editor.putString("User", json);
        editor.commit();
    }

    public void saveCompanyModel(CompanyModel companyModel) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(companyModel);
        editor.putString("Company", json);
        editor.commit();
    }

    public UserModel getUserModel() {
        Gson gson = new Gson();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        String json = preferences.getString("User", null);
        if (json == null) {
            Log.e(TAG, "getUserModel: "+"nullll");

            return null;
        }

        UserModel obj = gson.fromJson(json, UserModel.class);
        Log.e(TAG, "getUserModel: "+ obj.toString() );
        return obj;
    }


    public CompanyModel getCompanyModel() {
        Gson gson = new Gson();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String json = preferences.getString("Company", null);
        if (json == null) {
            Log.e(TAG, "getCompanyModel: "+"nullll");
            return null;
        }
        CompanyModel obj = gson.fromJson(json, CompanyModel.class);
        Log.e(TAG, "getCompanyModel: "+ obj.toString() );

        return obj;
    }

    public void clearAllData() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().clear().commit();

    }
}
