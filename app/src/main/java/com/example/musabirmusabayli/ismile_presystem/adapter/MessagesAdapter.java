package com.example.musabirmusabayli.ismile_presystem.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.listener.MessagesInteractionListener;
import com.example.musabirmusabayli.ismile_presystem.model.Message;
import com.example.musabirmusabayli.ismile_presystem.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class MessagesAdapter extends BaseAdapter {

    private static final String TAG = "MessagesAdapter";
    private LayoutInflater inflater;
    private ArrayList<Message> messages;
    private Context context;

    User currentUser;
    String uuid;
    MessagesInteractionListener callback;
    Activity activity;
    User user;


    public MessagesAdapter() {
        super();
    }

    public MessagesAdapter(Context context, ArrayList<Message> messages, MessagesInteractionListener callback, User user, Activity activity) {
        super();
        this.context = context;
        this.messages = messages;
        this.callback = callback;
        this.user = user;
        currentUser = user;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateChanged;
        TextView notification_message;

        LinearLayout leftMessage;
        TextView left_txtMsg;
        TextView left_msgTime;
        ImageView left_imgMsg;
        Button left_btn_send_location;
        RelativeLayout left_contact_message_holder;
        Button left_contact_message_photo;
        TextView left_contact_message_name;
        TextView left_contact_message_msisdn;
        RelativeLayout left_removed_message;
        //left audio
        RelativeLayout leftAudioMessageHolder;
        ImageView leftAudioDownloadBtn;
        ImageView leftAudioPlayBtn;
        ProgressBar leftAudioProgressBar;
        //left video
        ImageView leftVideoThumbImage;
        ImageView leftVideoPlayBtn;
        ImageView leftVideoDownloadBtn;
        ProgressBar leftVideoProgressBar;
        RelativeLayout leftVideoMessageHolder;

        LinearLayout rightMessage;
        TextView right_txtMsg;
        TextView right_msgTime;
        ImageView right_msgStatus_notSent;
        ImageView right_msgStatus_Sent;
        ImageView right_msgStatus_Read;
        ImageView right_imgMsg;
        RelativeLayout right_contact_message_holder;
        Button right_contact_message_photo;
        TextView right_contact_message_name;
        TextView right_contact_message_msisdn;
        RelativeLayout right_removed_message;
        //right video
        ImageView rightVideoThumbImage;
        ImageView rightVideoPlayBtn;
        ImageView rightVideoDownloadBtn;
        ImageView rightVideoUploadBtn;
        ProgressBar rightVideoProgressBar;
        RelativeLayout rightVideoMessageHolder;
        //right audio
        RelativeLayout rightAudioMessageHolder;
        ImageView rightAudioPlayBtn;
        ImageView rightAudioCancelBtn;
        ImageView rightAudioUploadBtn;
        ProgressBar rightAudioProgressBar;


        RelativeLayout left_message_reply;
        RelativeLayout right_message_reply;

        public ViewHolder(View view) {

            super(view);
        }

    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (messages.size() != 0) {
            final Message message = messages.get(position);
            final ViewHolder viewHolder;
            String timeOfMessage;
            boolean isChanged = false;
            SimpleDateFormat format = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date messageDate = new Date();
            try {
                messageDate = format.parse(message.getTime());
                Log.d("Message is ", "getView: " + message.toString());
                if (position == 0) {
                    isChanged = true;
                } else {
                    Date date2 = format.parse(messages.get(position - 1).getTime());
                    isChanged = isSameDay(messageDate, date2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            timeOfMessage = simpleDateFormat.format(messageDate);
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.message_list_item, null);

                viewHolder = new ViewHolder(convertView);

                viewHolder.dateChanged = (TextView) convertView.findViewById(R.id.date_change);
                viewHolder.notification_message = (TextView) convertView.findViewById(R.id.notification_msg);
                viewHolder.notification_message.setVisibility(View.GONE);

                viewHolder.leftMessage = (LinearLayout) convertView.findViewById(R.id.left_Message);
                viewHolder.left_txtMsg = (TextView) convertView.findViewById(R.id.left_txtMsg);

                final boolean[] isLongClick = new boolean[1];

                viewHolder.left_txtMsg.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        isLongClick[0] = true;
                        return false;
                    }
                });

                viewHolder.left_txtMsg.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP && isLongClick[0]) {
                            isLongClick[0] = false;
                            viewOnclickListenerForOpeningDialog(message);

                            return true;
                        }
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            isLongClick[0] = false;
                        }
                        return v.onTouchEvent(event);
                    }

                });
                viewHolder.left_msgTime = (TextView) convertView.findViewById(R.id.left_msgTime);
                viewHolder.left_imgMsg = (ImageView) convertView.findViewById(R.id.left_imgMsg);
                viewHolder.left_btn_send_location = (Button) convertView.findViewById(R.id.left_btn_send_location);
                viewHolder.left_contact_message_holder = (RelativeLayout) convertView.findViewById(R.id.left_contact_holder);
                viewHolder.left_contact_message_photo = (Button) convertView.findViewById(R.id.left_contact_photo);
                viewHolder.left_contact_message_name = (TextView) convertView.findViewById(R.id.left_contact_name);
                viewHolder.left_contact_message_msisdn = (TextView) convertView.findViewById(R.id.left_contact_msisdn);
                viewHolder.left_removed_message = (RelativeLayout) convertView.findViewById(R.id.left_message_removed_view_holder);
                viewHolder.leftAudioMessageHolder = (RelativeLayout) convertView.findViewById(R.id.leftAudioMessageHolder);
                viewHolder.leftAudioPlayBtn = (ImageView) convertView.findViewById(R.id.leftAudioPlayBtn);
                viewHolder.leftAudioDownloadBtn = (ImageView) convertView.findViewById(R.id.leftAudioDownloadBtn);
                viewHolder.leftAudioProgressBar = (ProgressBar) convertView.findViewById(R.id.leftAudioProgressBar);
                viewHolder.leftVideoMessageHolder = (RelativeLayout) convertView.findViewById(R.id.leftVideoMessageHolder);
                viewHolder.leftVideoDownloadBtn = (ImageView) convertView.findViewById(R.id.leftVideoDownloadBtn);
                viewHolder.leftVideoProgressBar = (ProgressBar) convertView.findViewById(R.id.leftVideoProgressBar);
                viewHolder.leftVideoPlayBtn = (ImageView) convertView.findViewById(R.id.leftVideoPlayBtn);
                viewHolder.leftVideoThumbImage = (ImageView) convertView.findViewById(R.id.leftVideoThumbImage);


                viewHolder.rightMessage = (LinearLayout) convertView.findViewById(R.id.right_Message);
                viewHolder.right_txtMsg = (TextView) convertView.findViewById(R.id.right_txtMsg);


                viewHolder.right_txtMsg.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        isLongClick[0] = true;
                        return false;
                    }
                });

                viewHolder.right_txtMsg.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP && isLongClick[0]) {
                            isLongClick[0] = false;
                            viewOnclickListenerForOpeningDialog(message);

                            return true;
                        }
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            isLongClick[0] = false;
                        }
                        return v.onTouchEvent(event);
                    }
                });

                viewHolder.right_msgTime = (TextView) convertView.findViewById(R.id.right_msgTime);
                viewHolder.right_msgStatus_notSent = (ImageView) convertView.findViewById(R.id.right_msgStatus_not_sent);
                viewHolder.right_msgStatus_Sent = (ImageView) convertView.findViewById(R.id.right_msgStatus_sent);
                viewHolder.right_msgStatus_Read = (ImageView) convertView.findViewById(R.id.right_msgStatus_read);
                viewHolder.right_imgMsg = (ImageView) convertView.findViewById(R.id.right_imgMsg);
                viewHolder.right_contact_message_holder = (RelativeLayout) convertView.findViewById(R.id.right_contact_holder);
                viewHolder.right_contact_message_photo = (Button) convertView.findViewById(R.id.right_contact_photo);
                viewHolder.right_contact_message_name = (TextView) convertView.findViewById(R.id.right_contact_name);
                viewHolder.right_contact_message_msisdn = (TextView) convertView.findViewById(R.id.right_contact_msisdn);
                viewHolder.right_removed_message = (RelativeLayout) convertView.findViewById(R.id.right_message_removed_view_holder);
                viewHolder.left_message_reply = (RelativeLayout) convertView.findViewById(R.id.left_message_reply);
                viewHolder.right_message_reply = (RelativeLayout) convertView.findViewById(R.id.right_message_reply);
                viewHolder.rightAudioMessageHolder = (RelativeLayout) convertView.findViewById(R.id.rightAudioMessageHolder);
                viewHolder.rightAudioPlayBtn = (ImageView) convertView.findViewById(R.id.rightAudioPlayBtn);
                viewHolder.rightAudioCancelBtn = (ImageView) convertView.findViewById(R.id.rightAudioCancelBtn);
                viewHolder.rightAudioUploadBtn = (ImageView) convertView.findViewById(R.id.rightAudioUploadBtn);
                viewHolder.rightAudioProgressBar = (ProgressBar) convertView.findViewById(R.id.rightAudioProgressBar);

                viewHolder.rightVideoMessageHolder = (RelativeLayout) convertView.findViewById(R.id.rightVideoMessageHolder);
                viewHolder.rightVideoDownloadBtn = (ImageView) convertView.findViewById(R.id.rightVideoDownloadBtn);
                viewHolder.rightVideoUploadBtn = (ImageView) convertView.findViewById(R.id.rightVideoUploadBtn);
                viewHolder.rightVideoPlayBtn = (ImageView) convertView.findViewById(R.id.rightVideoPlayBtn);
                viewHolder.rightVideoThumbImage = (ImageView) convertView.findViewById(R.id.rightVideoThumbImage);
                viewHolder.rightVideoProgressBar = (ProgressBar) convertView.findViewById(R.id.rightVideoProgressBar);

            }
            else
                viewHolder = (ViewHolder) convertView.getTag();

            convertView.setLongClickable(true);


            if (message.getText().endsWith("\n")) {
                message.setText(message.getText().substring(0, message.getText().length() - 1));
            }

            if (isChanged) {
                viewHolder.dateChanged.setVisibility(View.VISIBLE);
                String dateName = "";

                if (DateUtils.isToday(messageDate.getTime())) {
                    dateName = "Today";
                } else if (DateUtils.isToday(messageDate.getTime() + DateUtils.DAY_IN_MILLIS)) {
                    dateName = "Yesterday";
                } else {
                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy");
                    dateName = simpleDateFormat1.format(messageDate);
                }
                viewHolder.dateChanged.setText(dateName);

            } else {
                viewHolder.dateChanged.setVisibility(View.GONE);
            }


            if (currentUser.getMsisdn().equals(message.getFrom())) {

                Log.d(TAG, "getView: right");
                viewHolder.leftMessage.setVisibility(View.GONE);
                viewHolder.rightMessage.setVisibility(View.VISIBLE);
                viewHolder.notification_message.setVisibility(View.GONE);
                viewHolder.right_txtMsg.setVisibility(View.VISIBLE);
                viewHolder.right_txtMsg.setText(message.getText());
                viewHolder.right_imgMsg.setVisibility(View.GONE);
                viewHolder.rightVideoMessageHolder.setVisibility(View.GONE);
                viewHolder.right_contact_message_holder.setVisibility(View.GONE);
                viewHolder.right_removed_message.setVisibility(View.GONE);
                viewHolder.rightAudioMessageHolder.setVisibility(View.GONE);
                viewHolder.right_msgTime.setVisibility(View.VISIBLE);
                viewHolder.right_msgTime.setText(timeOfMessage);


            } else {
                Log.d("=============><>>","hereeeeeeeee");

// if message is incoming message show in left side
                viewHolder.rightMessage.setVisibility(View.GONE);
                viewHolder.leftMessage.setVisibility(View.VISIBLE);
                viewHolder.left_removed_message.setVisibility(View.GONE);
                viewHolder.notification_message.setVisibility(View.GONE);
                viewHolder.left_txtMsg.setText(message.getText());
                viewHolder.left_txtMsg.setVisibility(View.VISIBLE);
                viewHolder.left_btn_send_location.setVisibility(View.GONE);
                viewHolder.left_imgMsg.setVisibility(View.GONE);
                viewHolder.leftVideoMessageHolder.setVisibility(View.GONE);
                viewHolder.left_contact_message_holder.setVisibility(View.GONE);
                viewHolder.leftAudioMessageHolder.setVisibility(View.GONE);
                viewHolder.left_msgTime.setText(timeOfMessage);
                viewHolder.left_msgTime.setVisibility(View.VISIBLE);
            }

                View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        viewOnclickListenerForOpeningDialog(message);
                        return false;
                    }
                };
                convertView.setOnLongClickListener(longClickListener);
                viewHolder.left_txtMsg.setOnLongClickListener(longClickListener);
                viewHolder.left_imgMsg.setOnLongClickListener(longClickListener);
                viewHolder.left_contact_message_holder.setOnLongClickListener(longClickListener);
                viewHolder.right_txtMsg.setOnLongClickListener(longClickListener);
                viewHolder.right_imgMsg.setOnLongClickListener(longClickListener);
                viewHolder.right_contact_message_holder.setOnLongClickListener(longClickListener);
                convertView.setTag(viewHolder);

        }
        return convertView;
    }

    public void viewOnclickListenerForOpeningDialog(Message message) {
//        if (message.getMessage_type() == 0) {
//            if (message.getStatus().equals("[-1]")) {
//                alertForNotSentTextMessage(message);
//            } else {
//                alertForSentTextMessage(message);
//            }
//
//        } else if (message.getMessage_type() == 6) {
//            if (message.getStatus().equals("[-1]")) {
//                alertForNotSentContactMessage(message);
//            } else {
//                alertForSentContactMessage(message);
//            }
//
//        } else if (message.getMessage_type() == 1 || message.getMessage_type() == 3) {
//            if (message.getStatus().equals("[-1]")) {
//                alertForNotSentLocationAndImageMessage(message);
//            } else {
//                alertForSentLocationAndImageMessage(message);
//            }
//        }
    }


    private boolean isSameDay(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        boolean sameYear = calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
        boolean sameMonth = calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);
        boolean sameDay = calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH);
        System.out.println(!(sameDay && sameMonth && sameYear) + "   is date changed");
        return (!(sameDay && sameMonth && sameYear));
    }


    public void open_map(String text, String username) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + text + "?q=" + text + "(" + username + ")"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void alertForSentLocationAndImageMessage(final Message message) {
        final CharSequence[] items = {
                context.getString(R.string.forward_message),
                context.getString(R.string.replyMessage), "View", context.getString(R.string.delete)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            callback.onReplySelected(message, "");
                            break;
                        case 2:
                            if (message.getMessage_type() == 1) {


//                            Intent intent = new Intent(context, PhotoActivity.class);
                          //      String photoPath = AppConfig.URL_CHAT_PHOTO_BASE + currentUser.getMsisdn() + "/" + uuid + "/" + message.getId();
//                                    intent.putExtra("url", photoPath);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                               //s viewImage(photoPath, message);

                            } else if (message.getMessage_type() == 3) {
                                open_map(message.getText(), user.getName());
                            }
                            break;
                        case 3:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertForNotSentLocationAndImageMessage(final Message message) {
        final CharSequence[] items = {context.getString(R.string.forward_message),
                "View", "Delete"
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            if (message.getMessage_type() == 1) {


//                            Intent intent = new Intent(context, PhotoActivity.class);
//                                String photoPath = AppConfig.URL_CHAT_PHOTO_BASE + currentUser.getMsisdn() + "/" + uuid + "/" + message.getId();
////                                    intent.putExtra("url", photoPath);
////                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                                    context.startActivity(intent);
//                                viewImage(photoPath, message);

                            } else if (message.getMessage_type() == 3) {
                                open_map(message.getText(), user.getName());
                            }
                            break;
                        case 2:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void alertForSentTextMessage(final Message message) {
        final CharSequence[] items = {context.getString(R.string.forward_message),
                context.getString(R.string.replyMessage), context.getString(R.string.copy), context.getString(R.string.delete)
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            callback.onReplySelected(message, "");
                            break;
                        case 2:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clipData = ClipData.newPlainText("copy", message.getText());
                            clipboardManager.setPrimaryClip(clipData);
                            break;
                        case 3:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void alertForNotSentTextMessage(final Message message) {
        final CharSequence[] items = {context.getString(R.string.forward_message),
                context.getString(R.string.copy), context.getString(R.string.delete)
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clipData = ClipData.newPlainText("copy", message.getText());
                            clipboardManager.setPrimaryClip(clipData);
                            break;
                        case 2:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertForSentContactMessage(final Message message) {
        final CharSequence[] items = {context.getString(R.string.forward_message),
                context.getString(R.string.replyMessage), context.getString(R.string.delete)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            callback.onReplySelected(message, "");
                            break;
                        case 2:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertForNotSentContactMessage(final Message message) {
        final CharSequence[] items = {context.getString(R.string.forward_message),
                context.getString(R.string.delete)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                try {
                    Log.d(TAG, "onClick: clicked " + item);
                    switch (item) {
                        case 0:
                            callback.onForwardSelected(message);
                            break;
                        case 1:
                            if (message.getFrom().equals(currentUser.getMsisdn()))
                                callback.deleteMessage(message.getId(), true);
                            else
                                callback.deleteMessage(message.getId(), false);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }





}


