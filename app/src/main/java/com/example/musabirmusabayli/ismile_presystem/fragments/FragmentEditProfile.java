package com.example.musabirmusabayli.ismile_presystem.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.activity.ChangePasswordActivity;
import com.example.musabirmusabayli.ismile_presystem.dialog.CustomLogoutDialog;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;
import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.example.musabirmusabayli.ismile_presystem.model.UserModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FragmentEditProfile extends Fragment {

    private CompanyModel companyModel;
    private UserModel userModel;
    private String id;
    private CustomToastMessage c;
    public FragmentEditProfile() {
    }

    private View v;
    private ImageView logout;
    private TextView changePassword;
    private AppCompatEditText name, surname, email, description, phone;
    private AppCompatButton save;
    private SharedPreferenceHandler sharedPreferenceHandler;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profile, container, false);
        c = new CustomToastMessage(getActivity());
        sharedPreferenceHandler = new SharedPreferenceHandler(getActivity());
        logout = v.findViewById(R.id.logout);
        name = v.findViewById(R.id.name_txt);
        surname = v.findViewById(R.id.surname);
        email = v.findViewById(R.id.email);
        phone = v.findViewById(R.id.phone);
        description = v.findViewById(R.id.description);
        changePassword = v.findViewById(R.id.change_password);
        save = v.findViewById(R.id.save_button_edit_profile);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new CustomLogoutDialog(getActivity());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations_PauseDialogAnimation;
                dialog.show();
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkAllFields()) {
                    if (userModel != null) {
                        UserModel user = new UserModel();
                        user.setId(id);
                        user.setName(name.getText().toString().trim());
                        user.setSurname(surname.getText().toString().trim());
                        user.setEmail(email.getText().toString().trim());
                        user.setPhone(phone.getText().toString().trim());
                        user.setDescription(description.getText().toString().trim());
                        user.setPassword(userModel.getPassword());
                        user.setStatus(userModel.getStatus());
                        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("People");
                        ref1.child(id).setValue(user);
                        sharedPreferenceHandler.saveUserModel(user);
                        c.customToastMessage("Successfully changed");
                    }

                   else if (companyModel != null) {
                        CompanyModel user = new CompanyModel();
                        user.setId(id);
                        user.setCompanyName(name.getText().toString().trim());
                        user.setWebsite(surname.getText().toString().trim());
                        user.setEmail(email.getText().toString().trim());
                        user.setPhone(phone.getText().toString().trim());
                        user.setCompanyDescription(description.getText().toString().trim());
                        user.setPassword(companyModel.getPassword());
                        user.setStatus(companyModel.getStatus());

                        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("Companies");
                        ref1.child(id).setValue(user);
                        sharedPreferenceHandler.saveCompanyModel(user);
                        c.customToastMessage("Successfully changed");
                    }

                }
                else c.customToastMessage("Please, fill all the blanks!");
            }
        });

        if (sharedPreferenceHandler.getCompanyModel() != null) {
            companyModel = sharedPreferenceHandler.getCompanyModel();
            surname.setHint("Website");
            description.setHint("Company description");
            name.setText(companyModel.getCompanyName());
            email.setText(companyModel.getEmail());
            surname.setText(companyModel.getWebsite());
            phone.setText(companyModel.getPhone());
            description.setText(companyModel.getCompanyDescription());
            id = companyModel.getId();
        }
        if (sharedPreferenceHandler.getUserModel() != null) {
            userModel = sharedPreferenceHandler.getUserModel();
            name.setText(userModel.getName());
            email.setText(userModel.getEmail());
            surname.setText(userModel.getSurname());
            phone.setText(userModel.getPhone());
            description.setText(userModel.getDescription());

            id = userModel.getId();

        }
        return v;
    }

    private boolean checkAllFields() {
        return (checkEdittextIsNull(name) || checkEdittextIsNull(surname) || checkEdittextIsNull(email)
                || checkEdittextIsNull(phone) || checkEdittextIsNull(description));

    }

    private boolean checkEdittextIsNull(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

    private boolean checkPasswordMatch(String pas1, String pas2) {
        return pas1.equals(pas2);

    }
}
