package com.example.musabirmusabayli.ismile_presystem.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.adapter.ChatListAdapter;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.model.ChatModel;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;

/**
 * Created by Musabir on 4/27/2018.
 */

public class FragmentChat extends Fragment implements ObservableScrollViewCallbacks {

    EditText search_edittext;
    ObservableListView chat_list;
    private ChatListAdapter chatListAdapter;
    ArrayList<ChatModel> chatModels;
    private ArrayList<ChatModel> searchResults;
    private RelativeLayout search_lyt;
    private ImageView profile;
    private RelativeLayout write;
    View v;
    private Animation animHide,animShow;
    private CustomToastMessage customToastMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_chat, container, false);
        }
        profile = v.findViewById(R.id.profile);
        customToastMessage = new CustomToastMessage(getActivity());
        chat_list = v.findViewById(R.id.chat_list);
        write  = v.findViewById(R.id.write);
        chat_list.setScrollViewCallbacks(this);

        search_edittext = v.findViewById(R.id.search_edittext);
        search_lyt = v.findViewById(R.id.search_lyt);
        chatModels = new ArrayList<>();
        chatModels = fillLists();

        searchResults = new ArrayList<>();
        searchResults.addAll(chatModels);
        chatListAdapter = new ChatListAdapter(getActivity(),searchResults);
        chat_list.setAdapter(chatListAdapter);

        initAnimation();



        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customToastMessage.customToastMessage("The functionality has not implemented yet");
            }
        });

        search_edittext.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchResults.clear();
                searchResults.add(new ChatModel());
                for(int i=1;i<chatModels.size();i++){

                    if(chatModels.get(i).getProfileName().contains(s.toString().trim())||
                            chatModels.get(i).getMessage().contains(s.toString().trim())){
                        Log.d("-----<><>>>>>",chatModels.get(i).getProfileName());
                        searchResults.add(chatModels.get(i));
                    }
                }

                chatListAdapter = new ChatListAdapter(getActivity(),searchResults);
                chat_list.setAdapter(chatListAdapter);

            }
        });
        return v;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

    }

    @Override
    public void onDownMotionEvent() {

    }
    private void initAnimation()
    {
        animShow = AnimationUtils.loadAnimation( getActivity(), R.anim.slide_up_chat);
        animHide = AnimationUtils.loadAnimation( getActivity(), R.anim.slide_down_chat);
    }
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

        if (search_lyt == null) {
            return;
        }
        if (scrollState == ScrollState.UP) {
            Log.d("=======/./0"," uppp");

            if (search_lyt.getVisibility()==View.VISIBLE) {
                search_lyt.setVisibility(View.GONE);
                search_lyt.startAnimation( animHide );

            }
        } else if (scrollState == ScrollState.DOWN) {
            Log.d("=======/./0"," down");

            if (search_lyt.getVisibility()==View.GONE) {
                search_lyt.setVisibility(View.VISIBLE);
                search_lyt.startAnimation( animShow );

            }
        }
    }
     ArrayList<ChatModel> fillLists(){
        chatModels = new ArrayList<>();
        chatModels.add(new ChatModel());
        addChatItem(new ChatModel(2,"Kamran Sadiqov",
                "https://cdn.pixabay.com/photo/2016/03/09/15/10/man-1246508_960_720.jpg",
                "Salam","11:17 AM","+994558998855",1,false,false));
         addChatItem(new ChatModel(0,"Qalib Sarayev",
                "http://adas.cvc.uab.es/elektra/wp-content/uploads/sites/13/2016/01/david_vazquez.png",
                "Salam","11:16 AM","+994558998851",0,true,false));
         addChatItem(new ChatModel(0,"Test Group","http://www.reliancenipponlife.com/images/solutionsForGroup_banner.jpg",
                "Test","11:16 AM","+994558998851",0,true,false));
         addChatItem(new ChatModel(0,"Developer Group","http://www.developergroup.com/images/eac-pic-big4.jpg",
                "Hello Developers","11:15 AM","+994558998851",0,true,false));
         addChatItem(new ChatModel(1,"Hicret","https://amp.businessinsider.com/images/57636dfb52bcd029008c9cda-750-750.jpg",
                "Salam","11:15 AM","+994558998852",2,false,false));
         addChatItem(new ChatModel(2,"Bahram","https://amp.businessinsider.com/images/57636dfb52bcd029008c9cda-750-750.jpg",
                "Salam","11:14 AM","+994558998832",2,true,false));
         addChatItem(new ChatModel(1,"Saleh","http://adas.cvc.uab.es/elektra/wp-content/uploads/sites/13/2016/01/david_vazquez.png",
                "Salam","11:13 AM","+994558998335",1,true,false));
         addChatItem(new ChatModel(2,"Musabir Musabayli","https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&h=350",
                "Salam","11:12 AM","+994558998811",1,false,false));
        return chatModels;
    }

    void addChatItem(ChatModel chatModel){
        if(!findExistingOneFromList(chatModel.getMsisdn()))
            chatModels.add(chatModel);
    }
    boolean findExistingOneFromList(String msisdn){
        for(int i=0;i<chatModels.size();i++){
            if(chatModels.get(i)!=null &&chatModels.get(i).getMsisdn()!=null && chatModels.get(i).getMsisdn().equals(msisdn))
                return true;
        }
        return false;
    }
    public void clearAllList(){
        chatModels.clear();

    }
}
