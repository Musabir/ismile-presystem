package com.example.musabirmusabayli.ismile_presystem.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.activity.LoginActivity;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;


/**
 * Created by Musabir on 1/31/2018.
 */

public class CustomLogoutDialog extends Dialog {
    Context context;
    SharedPreferenceHandler sharedPreferenceHandler ;

    public CustomLogoutDialog(@NonNull Context context) {
        super(context);
        this.context = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_logout);
        sharedPreferenceHandler = new SharedPreferenceHandler(context);
        RelativeLayout yes = (RelativeLayout) findViewById(R.id.yes_txt_lyt);
        RelativeLayout no = (RelativeLayout) findViewById(R.id.no_txt_lyt);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferenceHandler.clearAllData();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

}
