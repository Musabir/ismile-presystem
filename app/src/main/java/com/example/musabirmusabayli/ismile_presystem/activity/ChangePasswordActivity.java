package com.example.musabirmusabayli.ismile_presystem.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;
import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.example.musabirmusabayli.ismile_presystem.model.UserModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class ChangePasswordActivity extends AppCompatActivity {

    private TextView _back_txt;
    private AppCompatEditText oldPassword, newPassword, confirmNewPassword;
    private String id, oldPasswordTxt;
    private AppCompatButton save_button;
    private CustomToastMessage customToastMessage;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private CompanyModel companyModel;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        _back_txt = findViewById(R.id._back_txt);
        oldPassword = findViewById(R.id.old_password);
        newPassword = findViewById(R.id.new_password);
        confirmNewPassword = findViewById(R.id.confirm_new_password);
        save_button = findViewById(R.id.save_button);
        customToastMessage = new CustomToastMessage(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkAllFields()) {
                    if(checkPasswordMatch(oldPasswordTxt, oldPassword.getText().toString().trim())) {
                        if (checkPasswordMatch(newPassword.getText().toString().trim(), confirmNewPassword.getText().toString().trim())) {
                            if (userModel != null) {
                                DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("People");
                                ref1.child(id).child("password").setValue(newPassword.getText().toString().trim());
                                customToastMessage.customToastMessage("Successfully changed");
                                onBackPressed();
                            } else if (companyModel != null) {
                                DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("Companies");
                                ref1.child(id).child("password").setValue(newPassword.getText().toString().trim());
                                customToastMessage.customToastMessage("Successfully changed");
                                onBackPressed();
                            }
                        } else
                            customToastMessage.customToastMessage("Passwords do not match");
                    }
                    else
                    customToastMessage.customToastMessage("Old password does not match");

                }
                else
                customToastMessage.customToastMessage("Please, fill all blanks");

            }
        });
        if (sharedPreferenceHandler.getCompanyModel() != null) {
            companyModel = sharedPreferenceHandler.getCompanyModel();

            id = companyModel.getId();
            oldPasswordTxt = companyModel.getPassword();
        }
        if (sharedPreferenceHandler.getUserModel() != null) {
            userModel = sharedPreferenceHandler.getUserModel();

            id = userModel.getId();
            oldPasswordTxt = userModel.getPassword();
        }

    }

    private boolean checkAllFields() {
        return (checkEdittextIsNull(oldPassword) || checkEdittextIsNull(newPassword)
                || checkEdittextIsNull(confirmNewPassword));

    }

    private boolean checkEdittextIsNull(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

    private boolean checkPasswordMatch(String pas1, String pas2) {
        return pas1.equals(pas2);

    }
}
