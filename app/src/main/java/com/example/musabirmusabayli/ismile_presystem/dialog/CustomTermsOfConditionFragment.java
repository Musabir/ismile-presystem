package com.example.musabirmusabayli.ismile_presystem.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musabirmusabayli.ismile_presystem.R;


/**
 * Created by Musabir on 11/8/2017.
 */

public class CustomTermsOfConditionFragment extends Dialog {


    public CustomTermsOfConditionFragment(@NonNull Context context) {
        super(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_terms_of_condition);


        TextView txt = (TextView) findViewById(R.id.ok_txt);
        TextView terms_of_condition = (TextView) findViewById(R.id.terms_of_condition);

        RelativeLayout ok_lyt = (RelativeLayout) findViewById(R.id.ok_lyt);
        ok_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}


