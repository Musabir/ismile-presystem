package com.example.musabirmusabayli.ismile_presystem.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.musabirmusabayli.ismile_presystem.R;


public class FragmentSearch extends Fragment {

    public FragmentSearch() {}
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        
        return view;
    }

}
