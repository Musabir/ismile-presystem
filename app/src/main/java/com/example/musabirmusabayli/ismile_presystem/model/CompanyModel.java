package com.example.musabirmusabayli.ismile_presystem.model;

public class CompanyModel {
    String companyName;
    String website;
    String email;
    String phone;
    String password;
    String companyDescription;
    int status;//0 not confirm; 1 confirm; 2 deleted
    String id;


    public CompanyModel(String companyName, String website, String email, String phone, String password, String companyDescription, int status, String id) {
        this.companyName = companyName;
        this.website = website;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.companyDescription = companyDescription;
        this.status = status;
        this.id = id;
    }

    public CompanyModel() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CompanyModel{" +
                "companyName='" + companyName + '\'' +
                ", website='" + website + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", companyDescription='" + companyDescription + '\'' +
                ", id='" + id + '\'' +
                ", status=" + status +
                '}';
    }
}
