package com.example.musabirmusabayli.ismile_presystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;
import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.example.musabirmusabayli.ismile_presystem.model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText email, password;
    private AppCompatCheckBox passwordShow;
    private TextView forgetPassword, companyRegistration, personRegistration;
    private RelativeLayout loginButton;
    private CustomToastMessage customToastMessage;
    boolean findUsersChecked = false;
    boolean findCompaniesChecked = false;
    boolean isFound = false;
    private SharedPreferenceHandler sharedPreferenceHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        customToastMessage = new CustomToastMessage(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        email = findViewById(R.id.loginEmail);
        password = findViewById(R.id.loginPassword);
        passwordShow = findViewById(R.id.show_hide_password);
        forgetPassword = findViewById(R.id.forgot_password);
        companyRegistration = findViewById(R.id.register_as_a_company);
        personRegistration = findViewById(R.id.register_as_a_person);
        loginButton = findViewById(R.id.loginSubmit);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().trim().length() > 0 && password.getText().toString().trim().length() > 0) {
                    isFound = false;
                    findUsersChecked = false;
                    findCompaniesChecked = false;
                    checkUserName();
                } else
                    customToastMessage.customToastMessage("Incorrect email or password!");

            }
        });

        companyRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationAsACompanyActivity.class);
                startActivity(intent);
            }
        });

        personRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationAsAPersonActivity.class);
                startActivity(intent);
            }
        });

        passwordShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            int start, end;

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    start = password.getSelectionStart();
                    end = password.getSelectionEnd();
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    ;
                    password.setSelection(start, end);
                } else {
                    start = password.getSelectionStart();
                    end = password.getSelectionEnd();
                    password.setTransformationMethod(null);
                    password.setSelection(start, end);
                }
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customToastMessage.customToastMessage("The functionality has not been implemented yet!");
            }
        });


    }

    private void checkUserName() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("People");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        findUser(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("Companies");
        ref1.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        findCompany(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private void findUser(DataSnapshot dataSnapshot) {
        findUsersChecked = true;
        checkAllListsChecked();
        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
            UserModel singleUser = userSnapshot.getValue(UserModel.class);

            if (singleUser.getEmail().equals(email.getText().toString().trim()) && singleUser.getPassword().equals(password.getText().toString())) {
                if (singleUser.getStatus() == 1 ||singleUser.getStatus() == 0 ) {

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    sharedPreferenceHandler.saveUserModel(singleUser);
                    startActivity(i);
                } else if (singleUser.getStatus() == 0) {
                    customToastMessage.customToastMessage("The account has not been confirmed yet");
                } else if (singleUser.getStatus() == 2) {
                    customToastMessage.customToastMessage("The account was deleted");
                }
                isFound = true;
                break;

            }
        }
    }


    private void findCompany(DataSnapshot dataSnapshot) {
        findCompaniesChecked = true;
        checkAllListsChecked();
        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {

            CompanyModel singleUser = userSnapshot.getValue(CompanyModel.class);

            if (singleUser.getEmail().equals(email.getText().toString().trim()) && singleUser.getPassword().equals(password.getText().toString())) {
                if (singleUser.getStatus() == 1) {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    sharedPreferenceHandler.saveCompanyModel(singleUser);
                    startActivity(i);
                } else if (singleUser.getStatus() == 0) {
                    customToastMessage.customToastMessage("The account has not been confirmed yet");
                } else if (singleUser.getStatus() == 2) {
                    customToastMessage.customToastMessage("The account was deleted");
                }
                isFound = true;

                break;
            }

        }
    }

    private void checkAllListsChecked() {
        if (findUsersChecked == true && findCompaniesChecked == true) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isFound)
                        customToastMessage.customToastMessage("Incorrect email or password!");
                }
            }, 200);

        }
    }


}
