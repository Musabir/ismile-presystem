package com.example.musabirmusabayli.ismile_presystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.TouchDelegate;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.dialog.CustomTermsOfConditionFragment;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.helper.NetworkConnectivity;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;
import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.example.musabirmusabayli.ismile_presystem.model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hanks.library.AnimateCheckBox;

public class RegistrationAsAPersonActivity extends AppCompatActivity {

    private EditText name,surname,email,phone,password,confirmPassword,description;
    private Button signUpBtn;
    private RelativeLayout back_rly;
    private CustomToastMessage customToastMessage;
    private AnimateCheckBox termsOfConditionAcceptCheckBox;
    private TextView termsOfConditionTxt;
    private boolean isTermsOfConditionAcceptted = false;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private boolean s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_as_aperson);
        customToastMessage = new CustomToastMessage(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        name = findViewById(R.id.name);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        description = findViewById(R.id.description);
        signUpBtn = findViewById(R.id.signUpPersonBtn);
        back_rly = findViewById(R.id.back_rly);
        termsOfConditionTxt = findViewById(R.id.terms_of_condition);
        termsOfConditionAcceptCheckBox = (AnimateCheckBox) findViewById(R.id.checkbox_image);
        final View parent = (View) termsOfConditionAcceptCheckBox.getParent();  // button: the view you want to enlarge hit area
        termsOfConditionAcceptCheckBox.setOnCheckedChangeListener(new AnimateCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(View buttonView, boolean isChecked) {
                if (isChecked == false) {
                    isTermsOfConditionAcceptted = false;
                } else {
                    isTermsOfConditionAcceptted = true;
                }
            }
        });
        parent.post(new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                termsOfConditionAcceptCheckBox.getHitRect(rect);
                rect.top -= 10;
                rect.left -= 10;
                rect.bottom += 10;
                rect.right += 10;
                parent.setTouchDelegate(new TouchDelegate(rect, termsOfConditionAcceptCheckBox));
            }
        });
        back_rly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTermsOfConditionAcceptted) {

                    if (checkAllFields() == false) {
                        if (checkPasswordMatch(password.getText().toString(),confirmPassword.getText().toString())) {
                            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("People");
                            ref1.addListenerForSingleValueEvent(
                                    new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            checkPerson(dataSnapshot);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });

                        } else customToastMessage.customToastMessage("Passwords do not match");

                    } else customToastMessage.customToastMessage("Please, fill all the blanks!");
                }
                else customToastMessage.customToastMessage("Read and accept terms and condition");
            }
        });


        termsOfConditionTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new CustomTermsOfConditionFragment(RegistrationAsAPersonActivity.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations_PauseDialogAnimation;
                dialog.show();
            }
        });

    }

    private boolean checkAllFields(){
        return (checkEdittextIsNull(name) ||checkEdittextIsNull(surname) ||checkEdittextIsNull(email)
                ||checkEdittextIsNull(phone) ||checkEdittextIsNull(description)
                ||checkEdittextIsNull(password) ||checkEdittextIsNull(confirmPassword));

    }
    private boolean checkEdittextIsNull(EditText editText){
        return editText.getText().toString().trim().length()==0;
    }
    private boolean checkPasswordMatch(String pas1, String pas2){
        return pas1.equals(pas2);

    }


    private void checkPerson(DataSnapshot dataSnapshot) {
        s = false;
        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {

            CompanyModel singleUser = userSnapshot.getValue(CompanyModel.class);

            if (singleUser.getEmail().equals(email.getText().toString().trim())) {
                s = true;
                customToastMessage.customToastMessage("The email already registered");
                break;
            }

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!s) {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("People");

                    String userId = mDatabase.push().getKey();
                    UserModel userModel = new UserModel(name.getText().toString().trim(), surname.getText().toString().trim()
                            , email.getText().toString().trim(), phone.getText().toString().trim(), password.getText().toString().trim(), description.getText().toString().trim(),0);
                    userModel.setId(userId);
                    mDatabase.child(userId).setValue(userModel);
                    if (NetworkConnectivity.isOnline(RegistrationAsAPersonActivity.this)) {
                        Toast.makeText(RegistrationAsAPersonActivity.this, "Successfully added", Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(RegistrationAsAPersonActivity.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        sharedPreferenceHandler.saveUserModel(userModel);
                        startActivity(i);

                    }
                }
            }
        }, 100);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
