package com.example.musabirmusabayli.ismile_presystem.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musabirmusabayli.ismile_presystem.R;

public class CustomToastMessage {


        Context context;
        public CustomToastMessage(Context context){
            this.context = context;
        }


    public void customToastMessage(String txt)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Call toast.xml file for toast layout
        View toast = inflater.inflate(R.layout.custom_toast, null);
        TextView toast_msg = toast.findViewById(R.id.toast_msg);
        toast_msg.setText(txt);
        Toast toastt = new Toast(context);

        // Set layout to toast
        toastt.setView(toast);

        toastt.setDuration(Toast.LENGTH_SHORT);
        toastt.show();
    }
}
