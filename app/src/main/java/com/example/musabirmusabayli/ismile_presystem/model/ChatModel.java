package com.example.musabirmusabayli.ismile_presystem.model;


public class ChatModel {
    int numberOfUnseenMessage;
    String profileName;
    String profileImage;
    String message;
    String date;
    String msisdn;
    boolean isGroup;
    int status;//0 no internet;1 unseen; 2 seen
    boolean iSend;
    public ChatModel(){}

    public ChatModel(int numberOfUnseenMessage, String profileName, String profileImage, String message, String date, String msisdn, int status, boolean iSend, boolean isGroup) {
        this.numberOfUnseenMessage = numberOfUnseenMessage;
        this.profileName = profileName;
        this.profileImage = profileImage;
        this.message = message;
        this.date = date;
        this.msisdn = msisdn;
        this.status = status;
        this.iSend = iSend;
        this.isGroup = isGroup;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getNumberOfUnseenMessage() {
        return numberOfUnseenMessage;
    }

    public void setNumberOfUnseenMessage(int numberOfUnseenMessage) {
        this.numberOfUnseenMessage = numberOfUnseenMessage;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isiSend() {
        return iSend;
    }

    public void setiSend(boolean iSend) {
        this.iSend = iSend;
    }
}
