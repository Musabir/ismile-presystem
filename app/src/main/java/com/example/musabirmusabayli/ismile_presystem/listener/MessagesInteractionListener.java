package com.example.musabirmusabayli.ismile_presystem.listener;


import com.example.musabirmusabayli.ismile_presystem.model.Message;

public interface MessagesInteractionListener {
    void sendSeen(String message_id);
    void sendIsSeen(String message_id);
    void resendMessage(Message message);
    void deleteMessage(String message_id, boolean can_delete_from_both);
    void onReplySelected(Message message, String sender_name);
    void onReplyWithLocation(Message message);
    void reUploadVideo(Message message);
    void downloadVideo(Message message);
    void onForwardSelected(Message message);
}
