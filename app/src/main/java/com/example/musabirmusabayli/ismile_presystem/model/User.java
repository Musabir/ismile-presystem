package com.example.musabirmusabayli.ismile_presystem.model;


public class User {
    private int id;
    private String msisdn;
    private String name;
    private String definedName;
    private String avatar;
    private int chatStatus;
    private String moodMessage;
    private int blockStatus;
    private int settingTyping;
    private int settingLastOnline;
    private int settingSeen;
    private int isRegistered;
    private int isMyContact;

    public User() {
    }

    public User(int id, String msisdn, String name, String definedName, String avatar, int chatStatus, String moodMessage, int blockStatus, int settingTyping, int settingLastOnline, int settingSeen, int isRegistered, int isMyContact) {
        this.id = id;
        this.msisdn = msisdn;
        this.name = name;
        this.definedName = definedName;
        this.avatar = avatar;
        this.chatStatus = chatStatus;
        this.moodMessage = moodMessage;
        this.blockStatus = blockStatus;
        this.settingTyping = settingTyping;
        this.settingLastOnline = settingLastOnline;
        this.settingSeen = settingSeen;
        this.isRegistered = isRegistered;
        this.isMyContact = isMyContact;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        if (name == null || name.length() == 0) {
            return msisdn;
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefinedName() {
        return definedName;
    }

    public void setDefinedName(String definedName) {
        this.definedName = definedName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(int chatStatus) {
        this.chatStatus = chatStatus;
    }

    public String getMoodMessage() {
        return moodMessage;
    }

    public void setMoodMessage(String moodMessage) {
        this.moodMessage = moodMessage;
    }

    public int getBlockStatus() {
        return blockStatus;
    }

    public void setBlockStatus(int blockStatus) {
        this.blockStatus = blockStatus;
    }

    public int getSettingTyping() {
        return settingTyping;
    }

    public void setSettingTyping(int settingTyping) {
        this.settingTyping = settingTyping;
    }

    public int getSettingLastOnline() {
        return settingLastOnline;
    }

    public void setSettingLastOnline(int settingLastOnline) {
        this.settingLastOnline = settingLastOnline;
    }

    public int getSettingSeen() {
        return settingSeen;
    }

    public void setSettingSeen(int settingSeen) {
        this.settingSeen = settingSeen;
    }

    public int getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(int isRegistered) {
        this.isRegistered = isRegistered;
    }

    public int getIsMyContact() {
        return isMyContact;
    }

    public void setIsMyContact(int isMyContact) {
        this.isMyContact = isMyContact;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", name='" + name + '\'' +
                ", definedName='" + definedName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", chatStatus=" + chatStatus +
                ", moodMessage='" + moodMessage + '\'' +
                ", blockStatus=" + blockStatus +
                ", settingTyping=" + settingTyping +
                ", settingLastOnline=" + settingLastOnline +
                ", settingSeen=" + settingSeen +
                ", isRegistered=" + isRegistered +
                ", isMyContact=" + isMyContact +
                '}';
    }
}
