package com.example.musabirmusabayli.ismile_presystem.model;


public class Message {

    private String id;
    private String conv_id;
    private String from;
    private String to;
    private int message_type;
    private String status;
    private String target;
    private String text;
    private String time;
    private String deleted;
    private String reply;

    public Message() {
    }


    public Message(String id, String conv_id, String from, String to, int message_type, String status, String target, String text, String time, String deleted) {
        this.id = id;
        this.conv_id = conv_id;
        this.from = from;
        this.to = to;
        this.message_type = message_type;
        this.status = status;
        this.target = target;
        this.text = text;
        this.time = time;
        this.deleted = deleted;
    }

    public Message(String id, String conv_id, String from, String to, int message_type, String status, String target, String text, String time, String deleted, String reply) {
        this.id = id;
        this.conv_id = conv_id;
        this.from = from;
        this.to = to;
        this.message_type = message_type;
        this.status = status;
        this.target = target;
        this.text = text;
        this.time = time;
        this.deleted = deleted;
        this.reply = reply;
    }
    public Message(String id, String from, String to, int message_type, String status, String text, String time) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.message_type = message_type;
        this.status = status;
        this.text = text;
        this.time = time;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConv_id() {
        return conv_id;
    }

    public void setConv_id(String conv_id) {
        this.conv_id = conv_id;
    }



    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getReply() {
        if(reply==null){
            return "0";
        }
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", conv_id='" + conv_id + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", message_type=" + message_type +
                ", status=" + status +
                ", target='" + target + '\'' +
                ", text='" + text + '\'' +
                ", time='" + time + '\'' +
                ", deleted=" + deleted +
                ", reply='" + reply + '\'' +
                '}';
    }
}
