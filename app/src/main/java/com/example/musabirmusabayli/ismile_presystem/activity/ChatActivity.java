package com.example.musabirmusabayli.ismile_presystem.activity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.adapter.MessagesAdapter;
import com.example.musabirmusabayli.ismile_presystem.dialog.ChatViewAttachementDialog;
import com.example.musabirmusabayli.ismile_presystem.helper.Defaults;
import com.example.musabirmusabayli.ismile_presystem.listener.MessagesInteractionListener;
import com.example.musabirmusabayli.ismile_presystem.model.Message;
import com.example.musabirmusabayli.ismile_presystem.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements MessagesInteractionListener {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.microfon_icon)
    ImageView microfonIcon;
    private ArrayList<Message> messages;
    private User user;
    private MessagesAdapter messagesAdapter;
    @BindView(R.id.backbutton)
    ImageView backbutton;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.online)
    TextView online;
    @BindView(R.id.more_options)
    ImageView moreOptions;
    @BindView(R.id.list_message)
    ListView listMessage;
    @BindView(R.id.attachment_icon)
    ImageView attachmentIcon;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.send_button)
    RelativeLayout microfon;
    @BindView(R.id.blockedText)
    RelativeLayout blockedText;
    boolean res = false;

    private String chatPersonAvatar, chatPersonName, chatPersonMsisdn, lastseen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);
        //EventBus.getDefault().register(this);

        user = new User();
        chatPersonName = getIntent().getStringExtra("name");
        lastseen = "Online";
        chatPersonMsisdn = getIntent().getStringExtra("msisdn");
        chatPersonAvatar = getIntent().getStringExtra("avatar");

        name.setText(chatPersonName);

        online.setText(lastseen);
        user.setName(Defaults.myName);
        user.setMsisdn(Defaults.myNumber);
        if (!getIntent().getBooleanExtra("new", false))
            messages = addMessages();
        else
            messages = new ArrayList<>();

        messagesAdapter = new MessagesAdapter(ChatActivity.this, messages, ChatActivity.this, user, ChatActivity.this);
        listMessage.setAdapter(messagesAdapter);


        loadImage(chatPersonAvatar, profileImage);
        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    microfonIcon.setImageResource(R.drawable.ic_send_message);
                } else
                    microfonIcon.setImageResource(R.drawable.ic_microphone);


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (getIntent().getBooleanExtra("isGroup", false))
//                    openGroupInfo();


            }
        });
    }

    //    public Message(String id, String from, String to, int message_type, String status, String text, String time) {
//        this.id = id;
//        this.from = from;
//        this.to = to;
//        this.message_type = message_type;
//        this.status = status;
//        this.text = text;
//        this.time = time;
//    }


    private ArrayList<Message> addMessages() {
        messages = new ArrayList<>();
        messages.add(new Message("1", Defaults.myNumber, chatPersonMsisdn, 0, "1", "salam", "2011-11-02T02:50:12.208Z"));
        messages.add(new Message("2", Defaults.myNumber, chatPersonMsisdn, 0, "1", "necesen?", "2011-11-02T02:50:12.208Z"));
        messages.add(new Message("3", Defaults.myNumber, chatPersonMsisdn, 0, "1", "salam", "2011-11-02T02:50:12.210Z"));
        messages.add(new Message("4", Defaults.myNumber, chatPersonMsisdn, 0, "1", "salam", "2011-11-02T02:50:12.211Z"));
        messages.add(new Message("5", Defaults.myNumber, chatPersonMsisdn, 0, "1", "salam", "2011-11-02T02:50:12.300Z"));
        messages.add(new Message("5", chatPersonMsisdn, Defaults.myNumber, 0, "1", "salam", "2011-11-02T02:53:12.699Z"));
//994775558899
        return messages;
    }

    @OnClick({R.id.backbutton, R.id.profile_image, R.id.more_options, R.id.attachment_icon, R.id.send_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backbutton:
                onBackPressed();
                break;
            case R.id.profile_image:

                break;
            case R.id.more_options:

                break;
            case R.id.attachment_icon:
                final Dialog dialog = new ChatViewAttachementDialog(ChatActivity.this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations_PauseDialogAnimation;
                dialog.show();
                break;
            case R.id.send_button:
                String messageText = message.getText().toString().trim();
                Log.d("---><>>", "Clicked");
                if (messageText.length() > 0) {
                    sendMessage(messageText);
                }

                message.setText("");

                break;
        }
    }

    @Override
    public void sendSeen(String message_id) {

    }

    @Override
    public void sendIsSeen(String message_id) {

    }

    @Override
    public void resendMessage(Message message) {

    }

    @Override
    public void deleteMessage(String message_id, boolean can_delete_from_both) {

    }

    @Override
    public void onReplySelected(Message message, String sender_name) {

    }

    @Override
    public void onReplyWithLocation(Message message) {

    }

    @Override
    public void reUploadVideo(Message message) {

    }

    @Override
    public void downloadVideo(Message message) {

    }

    @Override
    public void onForwardSelected(Message message) {

    }

    public void sendMessage(String messageText) {
        Log.d("-------><>>", "here");


        String tempId = String.valueOf(new Date().getTime());


        Message message = new Message();
        message.setText(messageText);
        message.setId(tempId);
        message.setFrom(Defaults.myNumber);
        message.setTo(chatPersonMsisdn);
        message.setMessage_type(0);
        message.setStatus("[-1]");
        message.setDeleted("0");
        message.setTarget("direct");

        Date messageDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        message.setTime(format.format(messageDate));
        messages.add(message);
        if (messagesAdapter != null)
            messagesAdapter.notifyDataSetChanged();

        listMessage.setSelection(messages.size() - 1);
        Log.d("-------><>>", "here");
        if (online.getText().toString().trim().equals("Online")) {
            online.setText("typing...");
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getMessage("test");
                    online.setText("Online");


                }
            }, 500);
        }


    }

    public void getMessage(String messageText) {
        Log.d("-------><>>", "here");


        String tempId = String.valueOf(new Date().getTime());


        Message message = new Message();
        message.setText(messageText);
        message.setId(tempId);
        message.setTo(Defaults.myNumber);
        message.setFrom(chatPersonMsisdn);
        message.setMessage_type(0);
        message.setStatus("[-1]");
        message.setDeleted("0");
        message.setTarget("direct");

        Date messageDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        message.setTime(format.format(messageDate));
        messages.add(message);
        if (messagesAdapter != null)
            messagesAdapter.notifyDataSetChanged();

        listMessage.setSelection(messages.size() - 1);
        Log.d("-------><>>", "here");
    }

    private void loadImage(String url, final ImageView imageView) {
        Glide.with(ChatActivity.this)
                .load(url)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(100, 100) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        imageView.setImageDrawable(circularBitmapDrawable);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {

                    }
                });

    }

//    @Subscribe
//    public void onEvent(ArrayList<SimpleContact> simpleContacts){
//
//        online.setText(simpleContacts.size()+"");
//
//    }

}
