package com.example.musabirmusabayli.ismile_presystem.activity;

import android.app.Dialog;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.TouchDelegate;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.dialog.CustomTermsOfConditionFragment;
import com.example.musabirmusabayli.ismile_presystem.helper.CustomToastMessage;
import com.example.musabirmusabayli.ismile_presystem.helper.NetworkConnectivity;
import com.example.musabirmusabayli.ismile_presystem.model.CompanyModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hanks.library.AnimateCheckBox;

public class RegistrationAsACompanyActivity extends AppCompatActivity {

    private EditText companyName, website, email, phone, password, confirmPassword, companyDescription;
    private Button signUpBtn;
    private RelativeLayout back_rly;
    private CustomToastMessage customToastMessage;
    private AnimateCheckBox termsOfConditionAcceptCheckBox;
    private TextView termsOfConditionTxt;
    private boolean isTermsOfConditionAcceptted = false;
    private boolean s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_as_acompany);
        customToastMessage = new CustomToastMessage(this);
        companyName = findViewById(R.id.company_name);
        website = findViewById(R.id.website);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirm_password);
        companyDescription = findViewById(R.id.company_description);
        signUpBtn = findViewById(R.id.signUpPersonBtn);
        back_rly = findViewById(R.id.back_rly);
        termsOfConditionTxt = findViewById(R.id.terms_of_condition);
        termsOfConditionAcceptCheckBox = (AnimateCheckBox) findViewById(R.id.checkbox_image);
        final View parent = (View) termsOfConditionAcceptCheckBox.getParent();  // button: the view you want to enlarge hit area
        termsOfConditionAcceptCheckBox.setOnCheckedChangeListener(new AnimateCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(View buttonView, boolean isChecked) {
                if (isChecked == false) {
                    isTermsOfConditionAcceptted = false;
                } else {
                    isTermsOfConditionAcceptted = true;
                }
            }
        });
        parent.post(new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                termsOfConditionAcceptCheckBox.getHitRect(rect);
                rect.top -= 10;
                rect.left -= 10;
                rect.bottom += 10;
                rect.right += 10;
                parent.setTouchDelegate(new TouchDelegate(rect, termsOfConditionAcceptCheckBox));
            }
        });
        back_rly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTermsOfConditionAcceptted) {

                    if (checkAllFields() == false) {
                        if (password.getText().toString().equals(confirmPassword.getText().toString())) {

                            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child("Companies");
                            ref1.addListenerForSingleValueEvent(
                                    new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            checkCompany(dataSnapshot);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });


                        } else customToastMessage.customToastMessage("Passwords do not match");
                    } else customToastMessage.customToastMessage("Please, fill all the blanks!");

                } else customToastMessage.customToastMessage("Read and accept terms and condition");

            }

        });


        termsOfConditionTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new CustomTermsOfConditionFragment(RegistrationAsACompanyActivity.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations_PauseDialogAnimation;
                dialog.show();
            }
        });

    }

    private boolean checkAllFields() {
        return ((companyName.getText().toString().trim().length() == 0)
                || (email.getText().toString().trim().length() == 0)
                || (phone.getText().toString().trim().length() == 0)
                || (companyDescription.getText().toString().trim().length() == 0)
                || (password.getText().toString().trim().length() == 0)
                || (confirmPassword.getText().toString().trim().length() == 0));

    }

    private void checkCompany(DataSnapshot dataSnapshot) {
        s = false;
        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {

            CompanyModel singleUser = userSnapshot.getValue(CompanyModel.class);

            if (singleUser.getEmail().equals(email.getText().toString().trim())) {
                s = true;
                customToastMessage.customToastMessage("The email already registered");
                break;
            }

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!s) {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Companies");

                    String userId = mDatabase.push().getKey();
                    CompanyModel companyModel = new CompanyModel(companyName.getText().toString().trim(), website.getText().toString().trim()
                            , email.getText().toString().trim(), phone.getText().toString().trim(), password.getText().toString().trim(), companyDescription.getText().toString().trim(), 0,userId);
                    mDatabase.child(userId).setValue(companyModel);
                    if (NetworkConnectivity.isOnline(RegistrationAsACompanyActivity.this)) {
                        Toast.makeText(RegistrationAsACompanyActivity.this,
                                "Registration is successfully done.Wait for the company confirmation!",
                                Toast.LENGTH_LONG).show();

                    }
                }
            }
        }, 100);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
