package com.example.musabirmusabayli.ismile_presystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.helper.SharedPreferenceHandler;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final SharedPreferenceHandler sharedPreferenceHandler = new SharedPreferenceHandler(this);
        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {

                                          if (sharedPreferenceHandler.getUserModel() != null || sharedPreferenceHandler.getCompanyModel() != null) {
                                              Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                                              i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                              startActivity(i);
                                          } else {
                                              Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                                              i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                              startActivity(i);
                                          }
                                      }
                                  },
                1300);
    }
}

