package com.example.musabirmusabayli.ismile_presystem.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.musabirmusabayli.ismile_presystem.R;


public class FragmentNotification extends Fragment {

    public FragmentNotification() {}
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_notification, container, false);

        return v;
    }
}
