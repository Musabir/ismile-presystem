package com.example.musabirmusabayli.ismile_presystem.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentChat;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentEditProfile;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentHome;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentNotification;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentSearch;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private ImageView imageView1,imageView2,imageView3,imageView4,imageView5;
    private TextView textView1,textView2,textView3,textView4,textView5;


    Fragment fragmentHome = new FragmentHome();
    Fragment fragmentNotification = new FragmentNotification();
    Fragment fragmentSearch = new FragmentSearch();
    Fragment fragmentChat = new FragmentChat();
    Fragment fragmentEditProfile = new FragmentEditProfile();
    Fragment active = fragmentHome;
    final FragmentManager fm = getSupportFragmentManager();


    private List<Integer> actionBarColor = new ArrayList<>();
    private List<Integer> statusBarColor = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);
        textView1 = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        textView5 = findViewById(R.id.textView5);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBottomMenuDesign(0);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBottomMenuDesign(1);
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBottomMenuDesign(2);
            }
        });
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBottomMenuDesign(3);
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBottomMenuDesign(4);
            }
        });

        fm.beginTransaction().add(R.id.content_frame, fragmentEditProfile, "5").hide(fragmentEditProfile).commit();
        fm.beginTransaction().add(R.id.content_frame, fragmentChat, "4").hide(fragmentChat).commit();
        fm.beginTransaction().add(R.id.content_frame, fragmentSearch, "3").hide(fragmentSearch).commit();
        fm.beginTransaction().add(R.id.content_frame, fragmentNotification, "2").hide(fragmentNotification).commit();
        fm.beginTransaction().add(R.id.content_frame,fragmentHome, "1").commit();
        changeBottomMenuDesign(0);

    }


    public void changeBottomMenuDesign(int page) {
        textView1.setTextColor(getResources().getColor( R.color.unselectedColor));
        textView2.setTextColor(getResources().getColor( R.color.unselectedColor));
        textView3.setTextColor(getResources().getColor( R.color.unselectedColor));
        textView4.setTextColor(getResources().getColor( R.color.unselectedColor));
        textView5.setTextColor(getResources().getColor( R.color.unselectedColor));

        textView1.setVisibility(View.GONE);
        textView2.setVisibility(View.GONE);
        textView3.setVisibility(View.GONE);
        textView4.setVisibility(View.GONE);
        textView5.setVisibility(View.GONE);



        switch (page) {
            case 0:
                fm.beginTransaction().hide(active).show(fragmentHome).commit();
                active = fragmentHome;

                //   displayView(new AllBookFragment(),"AllBookFragment");

                imageView1.setColorFilter(ContextCompat.getColor(this, R.color.selectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView2.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView3.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView4.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView5.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                textView1.setVisibility(View.VISIBLE);
                textView1.setTextColor(getResources().getColor( R.color.selectedColor));
                break;
            case 1:
                fm.beginTransaction().hide(active).show(fragmentNotification).commit();
                active = fragmentNotification;
                // displayView(new FavedFragment(),"FavedFragment");

                imageView2.setColorFilter(ContextCompat.getColor(this, R.color.selectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView1.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView3.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView4.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView5.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                textView2.setVisibility(View.VISIBLE);
                textView2.setTextColor(getResources().getColor( R.color.selectedColor));
                break;
            case 2:

                fm.beginTransaction().hide(active).show(fragmentSearch).commit();
                active = fragmentSearch;
                // displayView(new DownloadedFragment(),"DownloadedFragment");

                imageView3.setColorFilter(ContextCompat.getColor(this, R.color.selectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView1.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView2.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView4.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView5.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                textView3.setVisibility(View.VISIBLE);
                textView3.setTextColor(getResources().getColor( R.color.selectedColor));
                break;

            case 3:

                fm.beginTransaction().hide(active).show(fragmentChat).commit();
                active = fragmentChat;

                imageView4.setColorFilter(ContextCompat.getColor(this, R.color.selectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView1.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView2.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView3.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView5.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                textView4.setVisibility(View.VISIBLE);
                textView4.setTextColor(getResources().getColor( R.color.selectedColor));
                break;

            case 4:

                fm.beginTransaction().hide(active).show(fragmentEditProfile).commit();
                active = fragmentEditProfile;

                imageView5.setColorFilter(ContextCompat.getColor(this, R.color.selectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView1.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView2.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView3.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                imageView4.setColorFilter(ContextCompat.getColor(this, R.color.unselectedColor), android.graphics.PorterDuff.Mode.SRC_IN);
                textView5.setVisibility(View.VISIBLE);
                textView5.setTextColor(getResources().getColor( R.color.selectedColor));
                break;
        }
    }


}
