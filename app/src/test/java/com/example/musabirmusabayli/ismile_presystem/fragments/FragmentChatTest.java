package com.example.musabirmusabayli.ismile_presystem.fragments;

import com.example.musabirmusabayli.ismile_presystem.model.ChatModel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FragmentChatTest {

    @Test
    public void addChatItem() {
        FragmentChat fragmentChat = new FragmentChat();
        int size = fragmentChat.fillLists().size();
        assertEquals(fragmentChat.chatModels.size(),size);
    }

    @Test
    public void findExistingOneFromList() {
        FragmentChat fragmentChat = new FragmentChat();
        int size = fragmentChat.fillLists().size();
        ChatModel chatModel = new ChatModel(0,"JJJJJ",
                "saasas",
                "Hello","09:32 AM","+37256772388",1,false,false);
        fragmentChat.addChatItem(chatModel);
        fragmentChat.addChatItem(chatModel);
        assertEquals(size+1,fragmentChat.chatModels.size());
    }

    @Test
    public void clearAllList() {
        FragmentChat fragmentChat = new FragmentChat();
        fragmentChat.fillLists();
        fragmentChat.clearAllList();
        assertEquals(0,fragmentChat.chatModels.size());

    }
}