
package com.example.musabirmusabayli.ismile_presystem.activity;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.musabirmusabayli.ismile_presystem.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RegisterAsCompanyActivityTest {

    @Rule
    public ActivityTestRule<RegistrationAsACompanyActivity> mActivityTestRule = new ActivityTestRule<>(RegistrationAsACompanyActivity.class);
    Instrumentation.ActivityMonitor monitor = getInstrumentation()
            .addMonitor(MainActivity.class.getName(), null, false);

    @Test
    public void notFillAllBlanksTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());

        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));

    }


    @Test
    public void termsAndConditionNotCheckedTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Read and accept terms and condition")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));

    }

    @Test
    public void backButtonClick() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.back_rly)).perform(click());

    }

    @Test
    public void correctRegisterTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.company_name))
                .perform(typeText("Skpye"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("test1ss23@ttu.ee"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("12333221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText( "Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                        .check(matches(isDisplayed()));


    }


    @Test
    public void passwordDoesNotMatch() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skpye"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mumusa2212@ttu.ee"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("12333221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password1234"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());


        onView(withText("Passwords do not match")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
}
