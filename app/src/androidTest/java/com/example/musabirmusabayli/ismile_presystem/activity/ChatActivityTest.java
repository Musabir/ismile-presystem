package com.example.musabirmusabayli.ismile_presystem.activity;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.musabirmusabayli.ismile_presystem.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ChatActivityTest {

    @Rule
    public ActivityTestRule<ChatActivity> mActivityTestRule = new ActivityTestRule<>(ChatActivity.class);

    @Test
    public void checkMessageOnListview() {

        onView(withId(R.id.message))
                .perform(typeText("HELLO"), closeSoftKeyboard());
        onView(withId(R.id.send_button)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("HELLO")))
                .inAdapterView(withId(R.id.list_message));
    }
}
