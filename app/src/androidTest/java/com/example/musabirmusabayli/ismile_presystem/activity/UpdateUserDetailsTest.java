//package com.example.musabirmusabayli.ismile_presystem.activity;
//
//
//import android.support.test.espresso.ViewInteraction;
//import android.support.test.rule.ActivityTestRule;
//import android.support.test.runner.AndroidJUnit4;
//import android.test.suitebuilder.annotation.LargeTest;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ViewParent;
//
//import com.example.musabirmusabayli.ismile_presystem.R;
//
//import org.hamcrest.Description;
//import org.hamcrest.Matcher;
//import org.hamcrest.TypeSafeMatcher;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.Espresso.pressBack;
//import static android.support.test.espresso.action.ViewActions.click;
//import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
//import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
//import static android.support.test.espresso.action.ViewActions.replaceText;
//import static android.support.test.espresso.action.ViewActions.scrollTo;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//import static org.hamcrest.Matchers.allOf;
//import static org.hamcrest.Matchers.is;
//
//@RunWith(AndroidJUnit4.class)
//public class UpdateUserDetailsTest {
//
//    @Rule
//    public ActivityTestRule<SplashScreenActivity> mActivityTestRule = new ActivityTestRule<>(SplashScreenActivity.class);
//
//    @Test
//    public void updateUserDetailsTest() {
//        // Added a sleep statement to match the app's execution delay.
//        // The recommended way to handle such scenarios is to use Espresso idling resources:
//        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
//        try {
//            Thread.sleep(3596286);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        ViewInteraction textInputEditText = onView(
//                allOf(withId(R.id.loginEmail),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.lyt),
//                                        0),
//                                0),
//                        isDisplayed()));
//        textInputEditText.perform(click());
//
//        ViewInteraction textInputEditText2 = onView(
//                allOf(withId(R.id.loginEmail),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.lyt),
//                                        0),
//                                0),
//                        isDisplayed()));
//        textInputEditText2.perform(replaceText("mumusa@ttu.ee"), closeSoftKeyboard());
//
//        ViewInteraction textInputEditText3 = onView(
//                allOf(withId(R.id.loginPassword),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.lyt),
//                                        1),
//                                0),
//                        isDisplayed()));
//        textInputEditText3.perform(replaceText("Password123"), closeSoftKeyboard());
//
//        ViewInteraction textInputEditText4 = onView(
//                allOf(withId(R.id.loginPassword), withText("Password123"),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.lyt),
//                                        1),
//                                0),
//                        isDisplayed()));
//        textInputEditText4.perform(pressImeActionButton());
//
//        ViewInteraction relativeLayout = onView(
//                allOf(withId(R.id.loginSubmit),
//                        childAtPosition(
//                                allOf(withId(R.id.lyt),
//                                        childAtPosition(
//                                                withClassName(is("android.widget.RelativeLayout")),
//                                                2)),
//                                3),
//                        isDisplayed()));
//        relativeLayout.perform(click());
//
//        // Added a sleep statement to match the app's execution delay.
//        // The recommended way to handle such scenarios is to use Espresso idling resources:
//        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
//        try {
//            Thread.sleep(3429789);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        ViewInteraction appCompatImageView = onView(
//                allOf(withId(R.id.imageView4),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.buttommenu),
//                                        3),
//                                0),
//                        isDisplayed()));
//        appCompatImageView.perform(click());
//
//        ViewInteraction appCompatImageView2 = onView(
//                allOf(withId(R.id.imageView5),
//                        childAtPosition(
//                                childAtPosition(
//                                        withId(R.id.buttommenu),
//                                        4),
//                                0),
//                        isDisplayed()));
//        appCompatImageView2.perform(click());
//
//        ViewInteraction appCompatEditText = onView(
//                allOf(withId(R.id.surname), withText("Musabayli"),
//                        childAtPosition(
//                                allOf(withId(R.id.lyt),
//                                        childAtPosition(
//                                                withId(R.id.rly),
//                                                3)),
//                                1)));
//        appCompatEditText.perform(scrollTo(), replaceText("Mus"));
//
//        ViewInteraction appCompatEditText2 = onView(
//                allOf(withId(R.id.surname), withText("Mus"),
//                        childAtPosition(
//                                allOf(withId(R.id.lyt),
//                                        childAtPosition(
//                                                withId(R.id.rly),
//                                                3)),
//                                1),
//                        isDisplayed()));
//        appCompatEditText2.perform(closeSoftKeyboard());
//
//        pressBack();
//
//        ViewInteraction appCompatEditText3 = onView(
//                allOf(withId(R.id.surname), withText("Mus"),
//                        childAtPosition(
//                                allOf(withId(R.id.lyt),
//                                        childAtPosition(
//                                                withId(R.id.rly),
//                                                3)),
//                                1)));
//        appCompatEditText3.perform(scrollTo(), replaceText("Musa"));
//
//        ViewInteraction appCompatEditText4 = onView(
//                allOf(withId(R.id.surname), withText("Musa"),
//                        childAtPosition(
//                                allOf(withId(R.id.lyt),
//                                        childAtPosition(
//                                                withId(R.id.rly),
//                                                3)),
//                                1),
//                        isDisplayed()));
//        appCompatEditText4.perform(closeSoftKeyboard());
//
//        ViewInteraction appCompatButton = onView(
//                allOf(withId(R.id.save_button), withText("Save"),
//                        childAtPosition(
//                                allOf(withId(R.id.rly),
//                                        childAtPosition(
//                                                withClassName(is("android.widget.ScrollView")),
//                                                0)),
//                                4)));
//        appCompatButton.perform(scrollTo(), click());
//
//        ViewInteraction appCompatButton2 = onView(
//                allOf(withId(R.id.save_button), withText("Save"),
//                        childAtPosition(
//                                allOf(withId(R.id.rly),
//                                        childAtPosition(
//                                                withClassName(is("android.widget.ScrollView")),
//                                                0)),
//                                4)));
//        appCompatButton2.perform(scrollTo(), click());
//
//    }
//
//    private static Matcher<View> childAtPosition(
//            final Matcher<View> parentMatcher, final int position) {
//
//        return new TypeSafeMatcher<View>() {
//            @Override
//            public void describeTo(Description description) {
//                description.appendText("Child at position " + position + " in parent ");
//                parentMatcher.describeTo(description);
//            }
//
//            @Override
//            public boolean matchesSafely(View view) {
//                ViewParent parent = view.getParent();
//                return parent instanceof ViewGroup && parentMatcher.matches(parent)
//                        && view.equals(((ViewGroup) parent).getChildAt(position));
//            }
//        };
//    }
//}
