package com.example.musabirmusabayli.ismile_presystem.activity;


import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import com.example.musabirmusabayli.ismile_presystem.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);
    Instrumentation.ActivityMonitor monitor = getInstrumentation()
            .addMonitor(MainActivity.class.getName(), null, false);

    @Test
    public void incorrectLoginActivityTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.loginEmail))
                .perform(typeText("HELLO"), closeSoftKeyboard());
        onView(withId(R.id.loginPassword))
                .perform(typeText("HELLO"), closeSoftKeyboard());
        onView(withId(R.id.loginSubmit)).perform(click());

        onView(withText("Incorrect email or password!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));

    }

    @Test
    public void correctLoginActivityTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.loginEmail))
                .perform(typeText("mmm_musabir@mail.ru"), closeSoftKeyboard());
        onView(withId(R.id.loginPassword))
                .perform(typeText("12345678a"), closeSoftKeyboard());
        onView(withId(R.id.loginSubmit)).perform(click());

        Activity secondActivity = getInstrumentation()
                .waitForMonitorWithTimeout(monitor, 5000);
        assertNotNull(secondActivity);
        secondActivity.finish();
    }

    @Test
    public void openRegisterAsAPersonActivity(){
        Instrumentation.ActivityMonitor monitor = new Instrumentation.ActivityMonitor(RegistrationAsAPersonActivity.class.getName(), null, false);
        getInstrumentation().addMonitor(monitor);
        onView(withId(R.id.register_as_a_person)).check(matches(isDisplayed())).perform(click());

        Activity next = getInstrumentation().waitForMonitorWithTimeout(monitor, 2000);
        assertNotNull(next);
        assertEquals(1, monitor.getHits());

    }
    @Test
    public void openRegisterAsACompanyActivity(){
        Instrumentation.ActivityMonitor monitor = new Instrumentation.ActivityMonitor(RegistrationAsACompanyActivity.class.getName(), null, false);
        getInstrumentation().addMonitor(monitor);
        onView(withId(R.id.register_as_a_company)).check(matches(isDisplayed())).perform(click());

        Activity next = getInstrumentation().waitForMonitorWithTimeout(monitor, 2000);
        assertNotNull(next);
        assertEquals(1, monitor.getHits());

    }


}
