package com.example.musabirmusabayli.ismile_presystem.activity;

import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.musabirmusabayli.ismile_presystem.R;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentChat;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentEditProfile;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentHome;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentNotification;
import com.example.musabirmusabayli.ismile_presystem.fragments.FragmentSearch;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void openChatFragmentTest() {
        onView(withId(R.id.imageView4)).perform(click());
        FragmentChat fragment = new FragmentChat();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().
                add(R.id.content_frame, fragment).commit();

    }

    @Test
    public void openEditProfileFragmentTest() {
        onView(withId(R.id.imageView5)).perform(click());
        FragmentEditProfile fragment = new FragmentEditProfile();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();

    }

    @Test
    public void openHomeFragmentTest() {
        onView(withId(R.id.imageView)).perform(click());
        FragmentHome fragment = new FragmentHome();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();

    }

    @Test
    public void openNotificationFragmentTest() {
        onView(withId(R.id.imageView2)).perform(click());
        FragmentNotification fragment = new FragmentNotification();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();
    }

    @Test
    public void openSearchFragmentTest() {
        onView(withId(R.id.imageView3)).perform(click());
        FragmentSearch fragment = new FragmentSearch();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();
    }


    @Test
    public void editProfileFragmentEditUserNameTest() {
        onView(withId(R.id.imageView5)).perform(click());
        FragmentEditProfile fragment = new FragmentEditProfile();
        mActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
