
package com.example.musabirmusabayli.ismile_presystem.activity;

import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.musabirmusabayli.ismile_presystem.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RegisterAsCompanySubcomponentTest {

    @Rule
    public ActivityTestRule<RegistrationAsACompanyActivity> mActivityTestRule = new ActivityTestRule<>(RegistrationAsACompanyActivity.class);
    Instrumentation.ActivityMonitor monitor = getInstrumentation()
            .addMonitor(MainActivity.class.getName(), null, false);

    @Test
    public void insertOnlyCompanyNameTest() {
       

        onView(withId(R.id.company_name))
                .perform(typeText("Skype"), closeSoftKeyboard());

        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertOnlyCompanyNameAndEmailTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertOnlyCompanyNameEmailAndWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertOnlyCompanyNameEmailPhoneAndWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());


        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertOnlyCompanyNameEmailPhonePasswordAndWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("aa1221aa"), closeSoftKeyboard());


        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertOnlyCompanyNameEmailPhonePasswordConformPassowrdAndWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("aa1221aa"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("aa1221aa"), closeSoftKeyboard());


        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }


    @Test
    public void insertOnlyCompanyNameEmailPhonePasswordConformPassowrdCompanyDescriptionAndWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Read and accept terms and condition")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertCompanyNameContainsOnlySpaceTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("      "), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm1_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
    @Test
    public void insertEmptyCompanyNameTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("mmm_musabir@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
    @Test
    public void insertCompanyNameContainsOnlyDigitTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("12221"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype1@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertCompanyNameContainsDigitsAndLetterTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("1212Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype2@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());

        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
    @Test
    public void insertInvalidWebsiteNameTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype3@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid website")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }


    @Test
    public void insertInvalidWebsiteName2Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("http"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype4@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid website")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertEmptyWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype5@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertWebsiteContainsSpaceOnlyTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("  "), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype6@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
    @Test
    public void insertWrongWebsiteTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("www.skype.n"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype7@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));

    }

    @Test
    public void insertWrongWebsite2Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("ww.skype.n"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype8@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));

    }
        @Test
    public void insertEmptyEmailAddressTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertEmailAddressContainsOnlySpaceTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("              "), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidEmailAddressTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype8"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid email address")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidEmailAddress2Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype9@"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid email address")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidEmailAddress3Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype10@com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid email address")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidEmailAddress4Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("@gmaill.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid email address")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidEmailAddress5Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("@"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+994567332332"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid email address")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertPhoneNumberContainsTextTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("skype13"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid phone number")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertEmptyPhoneNumberTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype14@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }


    @Test
    public void insertPhoneNumberContainsOnlySpacesTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype15@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("       "), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidPhoneNumberTest() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype15@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid phone number")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidPhoneNumber2Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype16@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("1a"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid phone number")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidPhoneNumber3Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype17@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("a2"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid phone number")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertInvalidPhoneNumber4Test() {
       

        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype18@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("1+"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("Password123"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Invalid phone number")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertEmptyPasswordTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype19@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertPasswordContainsOnlySpacesTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype20@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("        "), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("        "), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertNotMatchingPasswordTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype21@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1221"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("2112"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Passwords do not match")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertPasswordContainsLetterOnlyTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype22@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("aaa"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("aaa"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("Registration is successfully done.Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertPasswordContainsDigitOnlyTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype23@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("The email already registered")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertPasswordContainsEverythingTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype24@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("I am just looking at around."), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());

        onView(withText("The email already registered")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertEmptyCompanyDescriptionTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype25@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertCompanyDescriptionContainsOnlySpaceTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype26@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("      "), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Please, fill all the blanks!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void insertCompanyDescriptionContainsOnlyDigitTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype27@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.checkbox_image)).perform(scrollTo()).perform(click());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Registration is successfully done. Wait for the company confirmation!")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void notAcceptTermsAndConditionTest() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype28@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Read and accept terms and condition")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void notAcceptTermsAndCondition1Test() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText("skype29@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Read and accept terms and condition")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Test
    public void notAcceptTermsAndCondition2Test() {
       
        onView(withId(R.id.company_name)).perform(scrollTo())
                .perform(typeText("Skype"), closeSoftKeyboard());
        onView(withId(R.id.website)).perform(scrollTo())
                .perform(typeText("https://skype.net"), closeSoftKeyboard());
        onView(withId(R.id.email)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(scrollTo())
                .perform(typeText("+12212221"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(scrollTo())
                .perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.confirm_password)).perform(scrollTo())
                .perform(typeText("1111!2wasA"), closeSoftKeyboard());
        onView(withId(R.id.company_description)).perform(scrollTo())
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.signUpPersonBtn)).perform(scrollTo()).perform(click());
        onView(withText("Read and accept terms and condition")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }
}
